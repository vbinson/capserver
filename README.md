# capserver

#### 介绍
海康威视 摄像头控制 目前实现了基本云台控制，有需要可以根据实际项目需要做调整。
项目使用springboot 框架+ 海康的SDK, 可以实现通过接口控制，或者使用监听MQTT 消息来实现云台控制。

#### 软件架构
软件架构说明 开发语言为java,框架使用springboot + 海康威视SDK


#### 安装教程

1.  打包需要注意，在window环境下面本地调试没有问题，如果需要部署到linux 环境，打包使用assembly 打包，使用zip包。
    打包流程请参考图片：
![输入图片说明](images1696661997902.png)
2.  使用zip 包部署到linux 系统中，解压zip包，运行对应的jar包。
   linux 解压后的效果：
   ![输入图片说明](images1696662237200.png)
    
3.  编写简单运行脚本，这里面只是一个例子，可以根据实际需要对脚本进行修改或者自己用自己方式部署。
    #!/bin/bash
   nohup java -server -Xms128M -Xmx128M -Xmn128M  -jar /xxx/aaaa/capserver-0.0.1-SNAPSHOT.jar &
   
运行就实现了海康威视的云台基本控制了。


#### 使用说明 （非开箱即用，有一定的学习成本）

1.  这个只是一个个人使用demo, 云台控制需要使用摄像头的端口地址以及账号密码，目前这些数据我是存放到mongodb 数据库中，如果自己实际项目用其他数据库，可以根据自己需要更改为自己的数据库。
2.  在实际开发过程中，出现过windows 环境可以正常运行调试，到了linux 系统部署后，出现海康的29 错误码，这是因为海康的动态库位置问题，咱们这个项目demo已经解决这个问题，他的动态库是我们zip包解压后的lib 目录里面。完美解决这个问题。
3.  还有一个海康的SDK 包中，有一个jna.jar 包，因为是本地引用，所以在打包的时候，我们这里会将这个jar包上传到你的本地仓库中去，这样你在打包的时候，这个jar包能够正常的引入到包中。
4. 安装部署的时候，把打好的zip包解压后，不要动解压后文件以及文件夹的位置，切记！
5. 调用说明
#### 第一个参数command 是摄像头控制的命令值。
#### 第一个参数key 是摄像头的序列号。
下面是基本的命令值：
![输入图片说明](image.png)


![输入图片说明](1696664936244.png)

#### 个人心得
我也是第一次接触海康威视sdk 的使用，里面是有一些坑，网上的解答也是比较少，都是我自己摸着石头过河，趟过这些坑，如果有这方面功能开发的小伙伴，可以白嫖我的思路，当然还有项目。如果使用中有什么问题，可以随时给我留言，我也会及时的帮你们进行问题处理。大家一起进步！

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
