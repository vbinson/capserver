package org.com.gr.capserver;

import lombok.extern.slf4j.Slf4j;
import org.com.gr.capserver.config.netty.BootNettyClientThread;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * ClassName: Application
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/6 19:28
 */
@Slf4j
@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class
, DataSourceAutoConfiguration.class
})
@EnableSwagger2
@EnableAsync
@EnableScheduling
public class CapServerApplication  implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication app =new SpringApplication(CapServerApplication.class);
        app.run(args);
        log.info("*******************************************");
        log.info("*                                         *");
        log.info("*           应用服务启动成功 ^_^             *");
        log.info("*                                         *");
        log.info("*******************************************");
    }
    @Async
    public void run(String... args) throws Exception {
        /**
         * 使用异步注解方式启动netty客户端服务
         */
       // int port = 5000;
        //String address = "192.168.3.26";
        //BootNettyClientThread thread = new BootNettyClientThread(port, address);
        //thread.start();
    }
}
