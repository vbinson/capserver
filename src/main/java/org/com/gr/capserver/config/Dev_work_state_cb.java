package org.com.gr.capserver.config;

import com.sun.jna.Pointer;
import org.com.gr.capserver.utils.hikvisionSDK.sdk.HCNetSDK;

/**
 * ClassName: Dev_work_state_cb
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/6 20:23
 */
public class Dev_work_state_cb implements HCNetSDK.DEV_WORK_STATE_CB {
    public boolean invoke(Pointer pUserdata, int iUserID, HCNetSDK.NET_DVR_WORKSTATE_V40 lpWorkState) {

        lpWorkState.read();
        System.out.println("设备状态:" + lpWorkState.dwDeviceStatic);
        for (int i = 0; i < HCNetSDK.MAX_CHANNUM_V40; i++) {
            int channel = i + 1;
            System.out.println("第" + channel + "通道是否在录像：" + lpWorkState.struChanStatic[i].byRecordStatic);
        }
        return true;
    }

}
