package org.com.gr.capserver.config;

import com.sun.jna.Pointer;
import org.com.gr.capserver.utils.hikvisionSDK.SDKBean;
import org.com.gr.capserver.utils.hikvisionSDK.sdk.HCNetSDK;
import org.springframework.util.StringUtils;

import java.util.concurrent.atomic.AtomicReference;

/**
 * ClassName: FExceptionCallBack_Imp
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/6 20:24
 */
public class FExceptionCallBack_Imp implements HCNetSDK.FExceptionCallBack {
    public void invoke(int dwType, int lUserID, int lHandle, Pointer pUser) {
        AtomicReference<String> key= new AtomicReference<>("");
        System.out.println("异常事件类型:" + dwType);
        SDKBean.lUserIDMap.entrySet().stream().forEach(entry->{
            if (entry.getValue().intValue()==lUserID){
                key.set(entry.getKey());
            }
        });
        if (!StringUtils.isEmpty(key.get())){
            SDKBean.lUserIDMap.remove(key.get());
        }
        return;
    }
}