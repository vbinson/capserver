package org.com.gr.capserver.config;

import com.sun.jna.Pointer;
import org.com.gr.capserver.utils.hikvisionSDK.sdk.HCNetSDK;

/**
 * ClassName: FlowTestcallback
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/6 20:22
 */
public class FlowTestcallback implements HCNetSDK.FLOWTESTCALLBACK {
    public void invoke(int lFlowHandle, HCNetSDK.NET_DVR_FLOW_INFO pFlowInfo,
                       Pointer pUser) {
        pFlowInfo.read();
        System.out.println("发送的流量数据：" + pFlowInfo.dwSendFlowSize);
        System.out.println("接收的流量数据：" + pFlowInfo.dwRecvFlowSize);
    }
}