package org.com.gr.capserver.config.logback;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;

import java.io.IOException;

/**
 * ClassName: LogBackExEncoder
 * Description:
 *
 * @author binbin_hao
 * @date 2023/8/3 14:29
 */
public class LogBackExEncoder extends PatternLayoutEncoder {
    static {
        PatternLayout.defaultConverterMap.put("T", ThreadNumConverter.class.getName());
        PatternLayout.defaultConverterMap.put("threadNum", ThreadNumConverter.class.getName());
    }
    public void doEncode(ILoggingEvent event) throws IOException {
        super.encode(event);
    }
}
