package org.com.gr.capserver.config.mqtt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

/**
 * ClassName: MqttClientCreateAuto
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/8 13:52
 */
@Component
public class MqttClientCreateAuto {
    @Resource
    private MqttClientManager mqttClientManager;
    @Autowired
    private MqttConfig mqttConfig;

    @PostConstruct
    public  void createMqttClient(){
        List<MqttClientProperties> mqttClientPropertiesList= mqttConfig.getClientList();
        for (MqttClientProperties properties : mqttClientPropertiesList){
            mqttClientManager.createMqttClient(properties);
        }
    }
}
