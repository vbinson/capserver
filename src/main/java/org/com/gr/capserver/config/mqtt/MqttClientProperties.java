package org.com.gr.capserver.config.mqtt;

import lombok.Data;

/**
 * ClassName: MqttClientProperties
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/8 9:58
 */
@Data
public class MqttClientProperties {
    /**
     * 客户端ID
     */
    private String clientId;
    /**
     * 客户端名称
     */
    private String clientName;
    /**
     * 客户端连接地址
     */
    private String url;

    /**
     * 订阅的主题
     */
    private String subscribeTopic;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 超时时间
     */
    private int timeout;
    /**
     * 保活时间
     */
    private int keepalive;
    /**
     * 默认qos
     */
    private int defaultQos;

    private boolean isShow;

    private boolean cleanSession;
}
