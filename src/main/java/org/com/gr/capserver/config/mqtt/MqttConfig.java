package org.com.gr.capserver.config.mqtt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * ClassName: MqttConfig
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/8 10:06
 */
@Data
@Configuration
@ConfigurationProperties(prefix="mqtt")
public class MqttConfig {

    List<MqttClientProperties> clientList;
}
