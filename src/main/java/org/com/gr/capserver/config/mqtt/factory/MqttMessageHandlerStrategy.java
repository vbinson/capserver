package org.com.gr.capserver.config.mqtt.factory;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import org.springframework.messaging.Message;

/**
 * ClassName: MqttMessageHandlerStrategyApp
 * Description: 不同设备 订阅主题 的策略 接口
 *
 * @author binbin_hao
 * @date 2023/7/17 10:22
 */
public interface MqttMessageHandlerStrategy {
    //获取主题
    String getTopic();
    //处理消息监听
    void handle(Message message);

    default  JSONObject mergeJson(JSONObject jsonObject1, JSONObject jsonObject2){
       try {
            for (String key: jsonObject2.keySet()){
            Object value2 = jsonObject2.get(key);
            if (jsonObject1.containsKey(key)){
                Object value1 = jsonObject1.get(key);
                if (!(value1 instanceof JSONObject)){
                    jsonObject1.put(key,value2);
                }else {
                    jsonObject1.put(key,mergeJson((JSONObject) value1,(JSONObject)value2));
                }
            }else {
                jsonObject1.put(key,value2);
            }
            }
            return jsonObject1;
      } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
