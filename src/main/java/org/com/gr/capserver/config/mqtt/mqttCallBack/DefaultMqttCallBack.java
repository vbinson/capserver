package org.com.gr.capserver.config.mqtt.mqttCallBack;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * ClassName: DefaultMqttCallBack
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/8 11:25
 */
@Component("default")
@Slf4j
public class DefaultMqttCallBack extends AbsMqttCallBack {
    @Override
    protected void handleReceiveMessage(String topic, Message commonMessage) {
        log.info("这里面处理监听的消息，topic:{}，message:{}",topic,commonMessage.getPayload());
    }
}
