package org.com.gr.capserver.config.mqtt.mqttCallBack;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ClassName: MqttCallBackContext
 * Description: 获取mqtt CallBack
 *
 * @author binbin_hao
 * @date 2023/9/8 11:36
 */
@Component
@Slf4j
public class MqttCallBackContext {
    private final Map<String, AbsMqttCallBack> callBackMap = new ConcurrentHashMap<>();

    /**
     * 定义private final Map spring 会把集成抽象类的AbsMqttCallBack 所有类封装为
     * Map 传过来，其中key 是Component("key") ,定义的key 的值
     * @param callBackMap
     */
    public  MqttCallBackContext(Map<String,AbsMqttCallBack> callBackMap){
        this.callBackMap.clear();
        callBackMap.forEach((k, v) -> this.callBackMap.put(k, v));
    }
    /**
     * 获取MQTT回调类
     *
     * @param clientName 客户端Name
     * @return MQTT回调类
     */
    public AbsMqttCallBack getCallBack(String clientName) {
        return this.callBackMap.get(clientName);
    }
}
