package org.com.gr.capserver.config.mqtt.mqttCallBack;

import lombok.extern.slf4j.Slf4j;
import org.com.gr.capserver.config.mqtt.factory.MqttMessageFactory;
import org.com.gr.capserver.config.mqtt.factory.MqttMessageHandlerStrategy;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * ClassName: AppMqttCallBack
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/8 14:07
 */
@Component("pcMqtt")
@Slf4j
public class PcMqttCallBack extends AbsMqttCallBack {

    protected void handleReceiveMessage(String topic, Message commonMessage) {
        log.info("这里面处理监听APP的消息，topic:{}，message:{}", topic, commonMessage.getPayload());
        MqttMessageHandlerStrategy mqttMessageHandlerStrategy= MqttMessageFactory.getMqttMessageHandler(topic);
        if (mqttMessageHandlerStrategy!=null){
            mqttMessageHandlerStrategy.handle(commonMessage);
        }else {
            log.info("这里面处理监听PC的消息，topic:{}，message:{},找不到对应的消息处理嘞", topic, commonMessage.getPayload());
        }
    }

}
