package org.com.gr.capserver.config.netty;


/**
 * ClassName: BootNettyClientThread
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/7 19:50
 */
public class BootNettyClientThread  extends Thread{
    private final int port;

    private final String address;
    public BootNettyClientThread(int port, String address){
        this.port = port;
        this.address = address;
    }

    public void run() {
        try {
            new BootNettyClient().connect(port, address);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
