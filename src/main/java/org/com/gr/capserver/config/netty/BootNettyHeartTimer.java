package org.com.gr.capserver.config.netty;




import io.netty.buffer.Unpooled;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: BootNettyHeartTimer
 * Description: 心跳使用定时器BootNettyHeartTimer
 *
 * @author binbin_hao
 * @date 2023/9/7 19:55
 */
@Service
public class BootNettyHeartTimer {

    //  使用定时器发送心跳
//    @Scheduled(cron = "0/30 * * * * ?")
//    public void heart_timer() {
//        String back = "heart";
//        if(BootNettyClientChannelCache.channelMapCache.size() > 0){
//            for (Map.Entry<String, BootNettyClientChannel> entry : BootNettyClientChannelCache.channelMapCache.entrySet()) {
//                BootNettyClientChannel bootNettyChannel = entry.getValue();
//                if(bootNettyChannel != null && bootNettyChannel.getChannel().isOpen()){
//                    bootNettyChannel.getChannel().writeAndFlush(Unpooled.buffer().writeBytes(back.getBytes()));
//                }
//            }
//        }
//
//    }


}