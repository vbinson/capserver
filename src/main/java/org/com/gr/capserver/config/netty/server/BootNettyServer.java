package org.com.gr.capserver.config.netty.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;

/**
 * ClassName: BootNettyServer
 * Description: netty server 包装类
 *
 * @author binbin_hao
 * @date 2023/9/20 17:12
 */
public class BootNettyServer {
    private final ServerBootstrap bootstrap;
    private final EventLoopGroup eventLoopGroupWorker;
    private final EventLoopGroup eventLoopGroupBoss;
    private int port;
    public BootNettyServer(int port) {
        eventLoopGroupWorker=new NioEventLoopGroup(2);
        eventLoopGroupBoss=new NioEventLoopGroup(1);
        bootstrap=new ServerBootstrap();
        port=port;
    }
    public void connect(){
        bootstrap.group(eventLoopGroupBoss,eventLoopGroupWorker)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG,1024)
                .option(ChannelOption.SO_REUSEADDR,true)
                .option(ChannelOption.SO_KEEPALIVE,false)
                .childOption(ChannelOption.TCP_NODELAY,true)
                .option(ChannelOption.SO_SNDBUF,65535)
                .option(ChannelOption.SO_RCVBUF,65555)
                .localAddress(new InetSocketAddress(port))
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new SocketInHandler());
                    }
                });

    }
}
