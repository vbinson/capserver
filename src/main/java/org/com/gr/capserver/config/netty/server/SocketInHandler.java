package org.com.gr.capserver.config.netty.server;

import io.netty.channel.*;
import org.com.gr.capserver.utils.StringUtils;

import java.net.InetSocketAddress;

/**
 * ClassName: SocketInHandler
 * Description: 入栈处理包装类
 *
 * @author binbin_hao
 * @date 2023/9/22 11:24
 */
@ChannelHandler.Sharable
public class SocketInHandler extends ChannelInboundHandlerAdapter {
    @Override

    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        Channel channel= ctx.channel();
        InetSocketAddress insocket = (InetSocketAddress)channel.remoteAddress();
        String host = insocket.getHostName();
        String clientIpAddress = StringUtils.formatIpAddress(host.equals("localhost") ? "127.0.0.1" :host,
                String.valueOf(insocket.getPort()));
    }
}
