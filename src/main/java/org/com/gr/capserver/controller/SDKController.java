package org.com.gr.capserver.controller;

import com.alibaba.fastjson.JSONObject;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.com.gr.capserver.config.mqtt.MqttClientProperties;
import org.com.gr.capserver.config.mqtt.MqttConfig;
import org.com.gr.capserver.config.netty.BootNettyClientChannel;
import org.com.gr.capserver.config.netty.BootNettyClientChannelCache;
import org.com.gr.capserver.model.Controller;
import org.com.gr.capserver.model.Login;
import org.com.gr.capserver.service.PTZHandlerService;
import org.com.gr.capserver.utils.ResultUtils;
import org.com.gr.capserver.utils.constant.BaseConstant;
import org.com.gr.capserver.utils.hikvisionSDK.SDKBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static org.com.gr.capserver.utils.constant.BaseConstant.COF_RESULT_DATA;

/**
 * ClassName: SDKController
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/7 8:16
 */
@Api(value = "摄像头云台控制")
@RestController
@Slf4j
@RequestMapping(BaseConstant.COF_BAS_URL + "/coframe/cap/platform")
public class SDKController {

    @Autowired
    SDKBean sdkBean;
    @Autowired
    MqttConfig mqttConfig;

    @Autowired
    PTZHandlerService ptzHandlerService;

    @ApiOperation("登陆")

    @PostMapping("/login")
    public ResultUtils login(@RequestBody Login login){
        String key=sdkBean.loginSDK(login.getIp(),login.getPort(),login.getUserName(), login.getPassword(),"");
        if (StringUtils.isEmpty(key)){
            return ResultUtils.fail("登陆失败");
        }
        return ResultUtils.ok().put(COF_RESULT_DATA,key);
    }
    @ApiOperation("登出")
    @PostMapping("/loginout")
    public ResultUtils loginout(@RequestBody String key){
        boolean isLoginOut=sdkBean.loginOutSDK(key);
        if (!isLoginOut){
            return ResultUtils.fail("登出失败");
        }
        return ResultUtils.ok();
    }
    @ApiOperation("控制开始")
    @PostMapping("/controllerstart")
    public ResultUtils controllerstart(@RequestBody Controller controller){
        return ptzHandlerService.controllerstart(controller);
    }
    @ApiOperation("控制结束")
    @PostMapping("/controllerend")
    public ResultUtils controllerend(@RequestBody Controller controller){
        boolean isLoginOut=sdkBean.controllerPanEnd(controller.getKey(),controller.getCommand(),controller.getSpeed());
        if (!isLoginOut){
            return ResultUtils.fail("控制开始失败");
        }
        return ResultUtils.ok();
    }
    @ApiOperation("测试")
    @GetMapping("/test")
    public ResultUtils testSendMessage(){
        System.out.println("进入测试");
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name","binson");
        for (Map.Entry<String, BootNettyClientChannel> entry : BootNettyClientChannelCache.channelMapCache.entrySet()) {
            BootNettyClientChannel bootNettyChannel = entry.getValue();
            if(bootNettyChannel != null && bootNettyChannel.getChannel().isOpen()){
                bootNettyChannel.getChannel().writeAndFlush(Unpooled.buffer().writeBytes(jsonObject.toJSONString().getBytes()));
            }
        }
        return ResultUtils.ok();
    }
    @ApiOperation("测试")
    @GetMapping("/test2")
    public ResultUtils test2(){
        System.out.println("进入测试");
        List<MqttClientProperties> list= mqttConfig.getClientList();
        list.stream().forEach(mqttClientProperties -> {
            log.info(JSONObject.toJSON(mqttClientProperties).toString());
        });
        return ResultUtils.ok();
    }
}
