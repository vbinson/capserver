package org.com.gr.capserver.model;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * ClassName: Cap
 * Description: 摄像头相关model
 *
 * @author binbin_hao
 * @date 2023/5/13 18:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("tb_cf_cap")
public class Cap implements Serializable {

    @ApiModelProperty("唯一标识")
    @JSONField(name = "_id")
    private String _id;

    @ApiModelProperty("租户（备用）")
    private String tenantId;

    @ApiModelProperty("摄像头唯一标识")
    private String cpIdx;


    @ApiModelProperty("摄像头名称")
    private String capName;

    @ApiModelProperty("菌菇大棚编号")
    private String groupIdx;


    @ApiModelProperty("摄像头IP")
    private String ip;

    @ApiModelProperty("摄像头参数，对外访问地址")
    private String para;

    @ApiModelProperty("是否显示 0不显示，1显示")
    private String showCtl;

    @ApiModelProperty("摄像头封面地址")
    private String videoImgUrl;

    @ApiModelProperty("模型Id（外键）")
    private String modelIds;

    @ApiModelProperty("创建时间")
    private String createTime;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("展示顺序")
    private Integer seq;

    @ApiModelProperty("app配置")
    private String appPara;

    @ApiModelProperty("序列号")
    private String serial;

    @ApiModelProperty("账号")
    private String username;

    @ApiModelProperty("password")
    private String password;

    @ApiModelProperty("port")
    private String port;

    @ApiModelProperty("摄像头类型：1普通，2鹰眼")
    private String capType;
}
