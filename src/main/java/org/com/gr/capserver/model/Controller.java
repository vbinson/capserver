package org.com.gr.capserver.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName: Controller
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/7 8:36
 */
@Data
public class Controller {

    @ApiModelProperty("登陆信息标识")
    private String key;

    @ApiModelProperty("控制指令")
    private int command;

    @ApiModelProperty("控制速度 速度： 0-7")
    private int speed;
}
