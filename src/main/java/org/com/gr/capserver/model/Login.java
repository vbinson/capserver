package org.com.gr.capserver.model;

import lombok.Data;

/**
 * ClassName: Login
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/7 8:25
 */
@Data
public class Login {
    String ip;
    int port;
    String userName;
    String password;
}
