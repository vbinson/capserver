package org.com.gr.capserver.model.enums;

/**
 * ClassName: SortEnum
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/8 15:48
 */
public enum SortEnum {
    //升序
    ASC(1,"asc"),

    //降序
    DESC(2,"desc");


    private int type;
    private String code;

    SortEnum(int type, String work) {
        this.type = type;
        this.code = work;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static SortEnum getByType(int type) {
        for (SortEnum constants : values()) {
            if (constants.getType() == type) {
                return constants;
            }
        }
        return null;
    }



}
