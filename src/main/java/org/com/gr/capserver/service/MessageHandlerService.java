package org.com.gr.capserver.service;


import org.com.gr.capserver.utils.ResultUtils;

/**
 * ClassName: MessageHandlerService
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/8 8:09
 */
public interface MessageHandlerService {
    public ResultUtils handlerMessage(String message);
}
