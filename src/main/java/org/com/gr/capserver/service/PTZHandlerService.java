package org.com.gr.capserver.service;

import org.com.gr.capserver.model.Controller;
import org.com.gr.capserver.utils.ResultUtils;

/**
 * ClassName: PTZHandlerService
 * Description: 摄像头控制service
 *
 * @author binbin_hao
 * @date 2023/9/8 17:48
 */
public interface PTZHandlerService {

    ResultUtils controllerstart(Controller controller);
}
