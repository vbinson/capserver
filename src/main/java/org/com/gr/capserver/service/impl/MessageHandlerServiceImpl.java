package org.com.gr.capserver.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.com.gr.capserver.service.MessageHandlerService;
import org.com.gr.capserver.utils.ResultUtils;
import org.springframework.stereotype.Service;

/**
 * ClassName: MessageHandlerServiceImpl
 * Description:
 *
 * @author binbin_hao
 * @date 2023/9/8 8:10
 */
@Service
@Slf4j
public class MessageHandlerServiceImpl implements MessageHandlerService {
    @Override
    public ResultUtils handlerMessage(String message) {
        System.out.println("正在处理message");
        log.info("消息处理完成：{}",message);
        return ResultUtils.ok();
    }
}
