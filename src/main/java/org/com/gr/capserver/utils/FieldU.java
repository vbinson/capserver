package org.com.gr.capserver.utils;

import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Function;

/**
 * ClassName: FieldU
 * Description: 提取实体类 字段名称
 *
 * @author binbin_hao
 * @date 2023/11/22 14:55
 */
public class FieldU {
    static String defaultSplit = "";
    static Integer defaultToType = 0;

    public FieldU() {
    }

    public static <T> String fName(SFunction<T, ?> fn) {
        return fName(fn, defaultSplit);
    }

    public static <T> String fName(SFunction<T, ?> fn, String split) {
        return fName(fn, split, defaultToType);
    }

    public static <T> String fName(SFunction<T, ?> fn, String split, Integer toType) {
        SerializedLambda serializedLambda = getSerializedLambda(fn);
        String fieldName = serializedLambda.getImplMethodName().substring("get".length());
        fieldName = fieldName.replaceFirst(fieldName.charAt(0) + "", (fieldName.charAt(0) + "").toLowerCase());

        Field field;
        try {
            field = Class.forName(serializedLambda.getImplClass().replace("/", ".")).getDeclaredField(fieldName);
        } catch (NoSuchFieldException | ClassNotFoundException var7) {
            throw new RuntimeException(var7);
        }

        TableField tableField = (TableField)field.getAnnotation(TableField.class);
        if (tableField != null && tableField.value().length() > 0) {
            return tableField.value();
        } else {
            switch(toType) {
                case 1:
                    return fieldName.replaceAll("[A-Z]", split + "$0").toUpperCase();
                case 2:
                    return fieldName.replaceAll("[A-Z]", split + "$0").toLowerCase();
                default:
                    return fieldName.replaceAll("[A-Z]", split + "$0");
            }
        }
    }

    private static <T> SerializedLambda getSerializedLambda(SFunction<T, ?> fn) {
        Method writeReplaceMethod;
        try {
            writeReplaceMethod = fn.getClass().getDeclaredMethod("writeReplace");
        } catch (NoSuchMethodException var6) {
            throw new RuntimeException(var6);
        }

        boolean isAccessible = writeReplaceMethod.isAccessible();
        writeReplaceMethod.setAccessible(true);

        SerializedLambda serializedLambda;
        try {
            serializedLambda = (SerializedLambda)writeReplaceMethod.invoke(fn);
        } catch (InvocationTargetException | IllegalAccessException var5) {
            throw new RuntimeException(var5);
        }

        writeReplaceMethod.setAccessible(isAccessible);
        return serializedLambda;
    }

    @Target({ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface TableField {
        String value() default "";
    }

    @FunctionalInterface
    public interface SFunction<T, R> extends Function<T, R>, Serializable {
    }
}
