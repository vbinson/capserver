package org.com.gr.capserver.utils;

/**
 * @Description
 * @Author xiaofeng
 * @CreateTime 2022/3/21
 * @Version 1.0
 */
public enum HttpCode {
    OK("响应成功","0000"),
    ERROR("异常","1001"),
    FAIL("失败","1002"),
    BAD_REQUEST("请求无效","400"),
    UNAUTHORIZED("未授权","401"),
    FORBIDDEN("无权访问","403"),
    ERR_REQUEST_TIME_OUT("请求超时","408"),
    INTERNAL_SERVER_ERROR("服务器内部错误","500"),
    SESSION_TIMEOUT("会话超时","9001");
















    private String name;
    private String code;

    private HttpCode(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public static String getName(String code) {
        for (HttpCode c : HttpCode.values()) {
            if (c.getCode().equals(code)) {
                return c.getName();
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }



}
