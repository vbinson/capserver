package org.com.gr.capserver.utils;

/**
 * ClassName: PageBo
 * Description:
 *
 * @author binbin_hao
 * @date 2023/11/22 15:08
 */
public class PageBo {
    private int pageIndex = 1;
    private int pageSize = 5;
    private int totalPage = 1;
    private int totalRow = 0;

    public PageBo() {
    }

    public PageBo(int pageIndex, int pageSize, int totalPage, int totalRow) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.totalPage = totalPage;
        this.totalRow = totalRow;
    }

    public int getPageIndex() {
        return this.pageIndex;
    }

    public PageBo setPageIndex(int pageIndex) {
        if (pageIndex > 0) {
            this.pageIndex = pageIndex;
        }

        return this;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public PageBo setPageSize(int pageSize) {
        if (pageSize > 0) {
            this.pageSize = pageSize;
        }

        return this;
    }

    public int getTotalPage() {
        return this.totalPage;
    }

    public PageBo setTotalPage(int totalPage) {
        if (totalPage > 0) {
            this.totalPage = totalPage;
        }

        return this;
    }

    public int getTotalRow() {
        return this.totalRow;
    }

    public PageBo setTotalRow(int totalRow) {
        this.totalRow = totalRow;
        return this;
    }

    public PageBo setTotalRow(long totalRow) {
        this.totalRow = (int)totalRow;
        return this;
    }

    public String toString() {
        return "PageBo{pageIndex=" + this.pageIndex + ", pageSize=" + this.pageSize + ", totalPage=" + this.totalPage + ", totalRow=" + this.totalRow + '}';
    }
}
