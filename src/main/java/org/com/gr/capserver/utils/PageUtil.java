package org.com.gr.capserver.utils;


import org.springframework.data.domain.Page;

import java.util.Objects;

import static org.com.gr.capserver.utils.constant.BaseConstant.*;


/**
 * ClassName: PageUtil
 * Description:
 *
 * @author binbin_hao
 * @date 2023/5/16 7:37
 */
public class PageUtil {
    public static <T> ResultUtils pageForResult(Page<T> page){
        PageBo pageBo= new PageBo().setPageIndex(page.getNumber())
                .setPageSize(page.getSize())
                .setTotalPage(page.getTotalPages())
                .setTotalRow(page.getTotalElements());
        return ResultUtils.ok().put(COF_RESULT_DATA,page.getContent()).put(COF_RESULT_PAGE,pageBo);
    }
    public static <T> ResultUtils pageForResult(Page<T> page,Object object){
        PageBo pageBo= new PageBo().setPageIndex(page.getNumber())
                .setPageSize(page.getSize())
                .setTotalPage(page.getTotalPages())
                .setTotalRow(page.getTotalElements());
        return ResultUtils.ok().put(COF_RESULT_DATA,page.getContent()).put(COF_RESULT_PAGE,pageBo).put(REQ_PARAM,object);
    }


    public static <E> ResultUtils pageForResultByMysql(com.github.pagehelper.Page<E> list) {
        return Objects.requireNonNull(ResultUtils.success("数据加载成功！")
                .put(COF_RESULT_DATA, list))
                .put(COF_RESULT_PAGE,new PageBo()
                        .setPageIndex(list.getPageNum())
                        .setPageSize(list.getPageSize())
                        .setTotalRow(list.getTotal())
                        .setTotalPage(list.getPages()));
    }
}
