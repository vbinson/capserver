package org.com.gr.capserver.utils;

/**
 * ClassName: PageVo
 * Description:
 *
 * @author binbin_hao
 * @date 2023/11/22 15:09
 */
public class PageVo {
    private int pageIndex = 1;
    private int pageSize = 5;

    public PageVo() {
    }

    public PageVo(int pageIndex, int pageSize) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

    public int getPageIndex() {
        return this.pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String toString() {
        return "PageVo{pageIndex=" + this.pageIndex + ", pageSize=" + this.pageSize + '}';
    }
}
