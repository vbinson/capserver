package org.com.gr.capserver.utils;

import com.alibaba.fastjson.JSON;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ClassName: ResultUtils
 * Description:
 *
 * @author binbin_hao
 * @date 2023/11/22 14:57
 */
public class ResultUtils extends ConcurrentHashMap<String, Object> {
    private static final String CODE = "rescode";
    private static final String MSG = "resmsg";
    private static final String EXECPTION = "exception";

    public ResultUtils() {
        super.put("rescode", HttpCode.OK.getCode());
        super.put("resmsg", "操作成功");
        super.put("exception", "");
    }

    public static ResultUtils ok() {
        return new ResultUtils();
    }

    public ResultUtils put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public static ResultUtils error(String msg) {
        return ok().put((String) "rescode", HttpCode.ERROR.getCode()).put((String) "resmsg", msg);
    }

    public static ResultUtils error(String msg, String errorException) {
        return ok().put((String) "rescode", HttpCode.ERROR.getCode()).put((String) "resmsg", msg).put((String) "exception", errorException);
    }

    public static ResultUtils error(String code, String msg, String errorException) {
        return ok().put((String) "rescode", code).put((String) "resmsg", msg).put((String) "exception", errorException);
    }

    public static ResultUtils fail(String msg) {
        return ok().put((String) "rescode", HttpCode.FAIL.getCode()).put((String) "resmsg", msg);
    }

    public static ResultUtils fail(String code, String msg) {
        return ok().put((String) "rescode", code).put((String) "resmsg", msg);
    }

    public static ResultUtils success(String msg) {
        return ok().put((String) "resmsg", msg);
    }

    public static void responseFailed(HttpServletResponse response, String msg) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String jsonString = JSON.toJSONString(fail(msg));
        writer.write(jsonString);
    }

    public static void responseFailed(HttpServletResponse response, String code, String msg) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String jsonString = JSON.toJSONString(fail(code, msg));
        writer.write(jsonString);
    }

    public static void responseError(HttpServletResponse response, String msg, String execption) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String jsonString = JSON.toJSONString(error(msg, execption));
        writer.write(jsonString);
    }

    public static void responseError(HttpServletResponse response, String code, String msg, String execption) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String jsonString = JSON.toJSONString(error(code, msg, execption));
        writer.write(jsonString);
    }

    public static void loginSessionTimeOut(HttpServletResponse response, String msg) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=UTF-8");
        response.setHeader("Content-Type", "application/json;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String jsonString = JSON.toJSONString(fail(msg).put((String) "rescode", HttpCode.SESSION_TIMEOUT.getCode()));
        writer.write(jsonString);
    }
}