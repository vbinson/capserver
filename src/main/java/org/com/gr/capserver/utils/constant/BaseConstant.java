package org.com.gr.capserver.utils.constant;


import java.util.HashMap;
import java.util.Map;

/**
 * @Description 常量定义
 * @Author xiaofeng
 * @CreateTime 2022/3/21
 * @Version 1.0
 */
public class BaseConstant {

    /*********************全局公共定义********************************/

    //全局接口 base url 地址 key
    public static final String  COF_BAS_URL = "grmfmrApi";

    //请求头携带 token 标识字段key
    public  static final String COF_HEADER_TOKEN = "Authorization";

    //请求头携带 token 标识字段key
    public  static final String TENANT_ID = "TenantId";

    //返回数据结果统一定义key值
    public static final String COF_RESULT_DATA = "data";
    public static final String COF_RESULT_METADATA = "metadata";
    public static final String COF_RESULT_COUNT = "count";

    //返回数据结果分页数据统一定义key值
    public static final String COF_RESULT_PAGE = "page";

    public static final String REQ_PARAM="reqParam";

    public static final String COF_PARAM_0 = "0";

    //所有树型结构数据对象根节点数据标识
    public static final String COF_TREE_ROOT="root";

    //用户默认初始化密码
    public static final String COF_USER_DEFAULT_PWD="0000#aaaa";



    /*******************JWT 用户数据封装 key 定义***********************/

    //用户信息key
    public static final String JWT_DATA_USER_KEY = "user";

    //用户角色key
    public static final String JWT_DATA_ROLE_KEY = "role";

    //用户权限
    public static final String  JWT_DATA_AUTHOR = "author";




    /*********************Redis 缓存定义********************************/

    //用户登录 redis 缓存
    public static final String REDIS_LOGIN_USER_CACHE_KEY = "grmfmrLogin:";

    //系统业务字典 redis 缓存定义key
    public static final String REDIS_DICT_CACHE_KEY = "grmfmrSysDict:cache";

    //锁定 redis 缓存定义
    public static final String REDIS_LOCKED_KEY = "mfmrLocked:cache";





    /*********************其他定义********************************/
    //验证码key
    public static final String CHECK_CODE="checkCode";






    /*************************服务内部调用结果 key值定义********************************/

    //公共数据返回结果内部嗲用定义
    public static final String  COM_RES_CODE="rescode";
    public static final String  COM_RES_MSG="resmsg";

    public static final String  COM_RES_EXECPTION="exception";

    public static final String AES_KEY = "1qaz2WSX3edc4RFV";

    public static final String AES_IV = "ZAQ!2wsxCDE#4rfv";



    /*************************初始化数据配置定义********************************/
    //存储脚本以及其他配置数据
    public static Map<String, String> scriptMap = new HashMap<>();
    static {
        //初始化用户批量导入模版文件配置
        scriptMap.put("importUserTemplate","importUserTemplate.xlsx");
        //初始化污染源管理批量导入模版文件配置
        scriptMap.put("importPollutionTemplate","importPollutionTemplate.xlsx");
    }

    public static Map<String,Boolean>  doNOtInterceptMap = new HashMap<>();
    //初始化添加不拦截配置地址
    static {
        // 添加swagger2 APi 访问不拦截
        doNOtInterceptMap.put("/doc.html",true);
        doNOtInterceptMap.put("/swagger-resources",true);
        doNOtInterceptMap.put("/webjars/bycdao-ui/images/api.ico",true);
        //静态资源目录第一级
        doNOtInterceptMap.put("webjars",true);
    }

    /*************************mqtt 协议地址 以及header********************************/
    //base topic 基础食用菌主题 正则接收的主题
    //mqtt 第一个+ 为 序列号 第二个 + 为 控制指令
    public static final String CAP_PTZ_TOPIC= "grmushroomApp/ptz/+/+/up";


    /***************************接收消息主题相关TOPIC设置***********************************************/


//    主题格式：
//    grmushroom/消息类别/机型/GRC0AD87895E8/设备类别/
//发送的主题(PC)
    /***************************接收消息主题相关 PC 下行数据TOPIC设置********************/






//    public static String getBaseTopic(String deviceName,boolean isApp){
//        if (isApp){
//            return MQTT_SEND_APP_BASE_TOPIC.replaceFirst("#","+")+"/"+deviceName+"/+";
//        }else {
//            return MQTT_SEND_BASE_TOPIC.replaceFirst("#","+")+"/"+deviceName+"/+";
//        }
//    }
//    grmushroom/消息类别/机型/GRC0AD87895E8/设备类别/

    public static void main(String[] args) {
        //System.out.println(getBaseTopic("device",false));
    }
}
